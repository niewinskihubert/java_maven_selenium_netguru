package com.netguru.automationpractice;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class TS_AT002_Login_Test {

    WebDriver driver;
    // Test Data
    String validEmail = "hubert.test01@trashmail.com";
    String validPassword = "hubert.test.password1";
    String invalidEmail = "hubert.testxx@trashmail.com";
    String invalidPassword = "hubert.test.passwordx";

    @BeforeEach
    public void driverSetup() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(15,TimeUnit.SECONDS);
        driver.manage().window().maximize();

        //Step 1
        driver.navigate().to("http://automationpractice.com/");
    }

    @AfterEach
    public void quitDriver() {driver.quit();}

    @Test
    public void TC_AT_Login_001() {

        // Verify whether user can log in with valid e-mail and password

        // Step 2 - click on the sign in button on the main page
        driver.findElement(By.xpath(".//a[@class='login']")).click();
        // Step 3 - enter valid e-mail
        driver.findElement(By.xpath(".//input[@id='email']")).sendKeys(validEmail);
        // Step 4 enter valid password
        driver.findElement(By.xpath(".//input[@id='passwd']")).sendKeys(validPassword);
        // Step 5 - click on sign in button
        driver.findElement(By.xpath(".//button[@id='SubmitLogin']")).click();
        // check whether user has successfully logged in
        String expectedUserName = "Test User";
        String actualUserName = driver.findElement(By.xpath(".//a/span[text()='Test User']")).getText();
        Assertions.assertEquals(expectedUserName, actualUserName,
                "User name is different than expected, login probably failed");
        // Step 6 - log out
        driver.findElement(By.xpath(".//a[@class='logout']")).click();
    }

    @Test
    public void TC_AT_Login_002() {
        // Verify whether user can't log in with valid e-mail and invalid password
        // Step 2 - click on the sign in button on the main page
        driver.findElement(By.xpath(".//a[@class='login']")).click();
        // Step 3 - enter valid e-mail
        driver.findElement(By.xpath(".//input[@id='email']")).sendKeys(validEmail);
        // Step 4 enter invalid password
        driver.findElement(By.xpath(".//input[@id='passwd']")).sendKeys(invalidPassword);
        //Step 5 - click on sign in button
        driver.findElement(By.xpath(".//button[@id='SubmitLogin']")).click();
        // verify whether user hasn't logged in
        String expectedErrorMessage = "Authentication failed.";
        String actualErrorMessage = driver.findElement(By.xpath(".//div[@class='alert alert-danger']/ol")).getText();
        Assertions.assertEquals(expectedErrorMessage,actualErrorMessage,
                "Error message is different than expected");
    }

    @Test
    public void TC_AT_Login_003() {
        // Verify whether user can't log in with invalid e-mail and valid password
        // Step 2 - click on the sign in button on the main page
        driver.findElement(By.xpath(".//a[@class='login']")).click();
        // Step 3 - enter invalid e-mail
        driver.findElement(By.xpath(".//input[@id='email']")).sendKeys(invalidEmail);
        // Step 4 enter valid password
        driver.findElement(By.xpath(".//input[@id='passwd']")).sendKeys(validEmail);
        // Step 5 - click on sign in button
        driver.findElement(By.xpath(".//button[@id='SubmitLogin']")).click();
        // verify whether user hasn't logged in
        String expectedErrorMessage = "Authentication failed.";
        String actualErrorMessage = driver.findElement(By.xpath(".//div[@class='alert alert-danger']/ol")).getText();
        Assertions.assertEquals(expectedErrorMessage, actualErrorMessage,
                "Error message is different than expected");
    }

    @Test
    public void TC_AT_Login_004() {
        // verify whether user can't log in with invalid e-mail and invalid password
        // Step 2 - click on the sign in button on the main page
        driver.findElement(By.xpath(".//a[@class='login']")).click();
        // Step 3 - enter invalid e-mail
        driver.findElement(By.xpath(".//input[@id='email']")).sendKeys(invalidEmail);
        // Step 4 enter invalid password
        driver.findElement(By.xpath(".//input[@id='passwd']")).sendKeys(invalidPassword);
        // Step 5 - click on sign in button
        driver.findElement(By.xpath(".//button[@id='SubmitLogin']")).click();
        // verify whether user hasn't logged in
        String expectedErrorMessage = "Authentication failed.";
        String actualErrorMessage = driver.findElement(By.xpath(".//div[@class='alert alert-danger']/ol")).getText();
        Assertions.assertEquals(expectedErrorMessage, actualErrorMessage,
                "Error message is different than expected");
    }

}
