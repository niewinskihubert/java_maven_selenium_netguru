package com.netguru.automationpractice;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class TS_AT001_Registration_Test {

    WebDriver driver;
    // Test Data
    Random random = new Random();
    String emailTest1 = "hubert.test" + random.nextInt(10000) + 10000 + "@trashmail.com";
    String emailTest2 = "hubert.test01@trashmail.com";
    String emailTest3 = "hubert.test03@trashmail.com";
    String firstName = "John";
    String lastName = "Doe";
    String password = "test.password1";
    String birthDay = "1";
    String birthMonth = "1";
    String birthYear = "1980";
    String companyName = "Test Company";
    String address = "Main Street";
    String address2 = "1";
    String city = "New York";
    String state = "New York";
    String postalCode = "11111";
    String country = "United States";
    String mobilePhone = "123456789";

    @BeforeEach
    public void driverSetup() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(20,TimeUnit.SECONDS);
        driver.manage().window().maximize();

        // Step 1
        driver.navigate().to("http://automationpractice.com/");
    }

    @AfterEach
    public void quitDriver() {driver.quit();}

    @Test
    public void TC_AT_Registration_001() {

        // verify whether user can register with valid e-mail and personal data

        // Step 2 - navigate to registration page
        driver.findElement(By.xpath(".//a[@class='login']")).click();
        // Step 3 - enter email
        driver.findElement(By.xpath(".//input[@id='email_create']")).sendKeys(emailTest1);
        // Step 4 - click on create an account
        driver.findElement(By.xpath(".//button[@id='SubmitCreate']")).click();

        // Step 5 - fill in the form
        // title
        driver.findElement(By.xpath(".//input[@id='id_gender1']")).click();
        // first name
        driver.findElement(By.xpath(".//input[@id='customer_firstname']")).sendKeys(firstName);
        // last name
        driver.findElement(By.xpath(".//input[@id='customer_lastname']")).sendKeys(lastName);
        // password
        driver.findElement(By.xpath(".//input[@id='passwd']")).sendKeys(password);
        // date of birth
        WebElement dayOfBirth = driver.findElement(By.xpath(".//select[@id='days']"));
        Select dayOfBirthDropdown = new Select(dayOfBirth);
        dayOfBirthDropdown.selectByValue(birthDay);
        WebElement monthOfBirth = driver.findElement(By.xpath(".//select[@id='months']"));
        Select monthOfBirthDropdown = new Select(monthOfBirth);
        monthOfBirthDropdown.selectByValue(birthMonth);
        WebElement yearOfBirth = driver.findElement(By.xpath(".//select[@id='years']"));
        Select yearOfBirthDropdown = new Select(yearOfBirth);
        yearOfBirthDropdown.selectByValue(birthYear);
        // first name
        driver.findElement(By.xpath(".//input[@id='firstname']")).sendKeys(firstName);
        // last name
        driver.findElement(By.xpath(".//input[@id='lastname']")).sendKeys(lastName);
        // company name
        driver.findElement(By.xpath(".//input[@id='company']")).sendKeys(companyName);
        // address
        driver.findElement(By.xpath(".//input[@id='address1']")).sendKeys(address);
        // address 2
        driver.findElement(By.xpath(".//input[@id='address2']")).sendKeys(address2);
        // city
        driver.findElement(By.xpath(".//input[@id='city']")).sendKeys(city);
        // country
        WebElement countryOfResidence = driver.findElement(By.xpath(".//select[@id='id_country']"));
        Select countryOfResidenceDropdown = new Select(countryOfResidence);
        countryOfResidenceDropdown.selectByVisibleText(country);
        // state
        WebElement stateOfResidence = driver.findElement(By.xpath(".//select[@id='id_state']"));
        Select stateOfResidenceDropdown = new Select(stateOfResidence);
        stateOfResidenceDropdown.selectByVisibleText(state);
        // postal code
        driver.findElement(By.xpath(".//input[@id='postcode']")).sendKeys(postalCode);
        // mobile phone
        driver.findElement(By.xpath(".//input[@id='phone_mobile']")).sendKeys(mobilePhone);
        // Step 6 - submit the form
        driver.findElement(By.xpath(".//button[@id='submitAccount']")).click();
        // Step 7 - verify whether user has successfully registered
        String expectedUserName = "John Doe";
        String actualUserName = driver.findElement(By.xpath(".//a[@class='account']")).getText();
        Assertions.assertEquals(expectedUserName, actualUserName,
                "Displayed username is different than expected, possibly failed Registration");
    }

    @Test
    public void TC_AT_Registration_002() {

        // verify whether user can't register without filling in the form

        // Step 1 - enter E-mail
        driver.findElement(By.xpath(".//a[@class='login']")).click();
        driver.findElement(By.xpath(".//input[@id='email_create']")).sendKeys(emailTest2);
        driver.findElement(By.xpath(".//button[@id='SubmitCreate']")).click();

        String expectedErrorMessage = "An account using this email address has already been registered." +
                " Please enter a valid password or request a new one.";
        String actualErrorMessage = driver.findElement(By.xpath(".//ol/li")).getText();
        Assertions.assertEquals(expectedErrorMessage, actualErrorMessage,
                "actual message is different than expected, possible registration defect");
    }


    @Test
    public void TC_AT_Registration_003() {

        // verify whether user can't register with already used e-mail

        // Step 2 - navigate to registration page
        driver.findElement(By.xpath(".//a[@class='login']")).click();
        // Step 3 - enter email
        driver.findElement(By.xpath(".//input[@id='email_create']")).sendKeys(emailTest3);
        // Step 4 - click on create an account
        driver.findElement(By.xpath(".//button[@id='SubmitCreate']")).click();
        // Step 5 - do not fill in the form
        // Step 6 - try to submit the form
        driver.findElement(By.xpath(".//button[@id='submitAccount']")).click();
        // check whether error message is displayed
        String expectedMessage = "There are 8 errors";
        String actualMessage = driver.findElement(By.xpath(".//div[@class='alert alert-danger']")).getText();
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }
}
